const express = require("express");
const router = express.Router();
const otherRouter = require("./other");
const userRouter = require("./user");

router.use("/", otherRouter);
router.use("/", userRouter);
module.exports = router;
