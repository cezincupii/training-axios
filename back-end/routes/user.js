const express = require("express");
const router = express.Router();
const userController = require("../controllers").user;

router.post("/addUser", userController.addUser);
router.get("/getUsers", userController.getUsers);
router.put("/updateUser", userController.updateUser);
router.post("/deleteUser", userController.deleteUser);
module.exports = router;
