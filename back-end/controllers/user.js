const express = require("express");
const UserDB = require("../models").User;
const controller = {
  addUser: async (req, res) => {
    const user = {
      nume: req.body.nume,
      prenume: req.body.prenume,
      email: req.body.email
    };
    console.log(user);
    await UserDB.create(user)
      .then(() => {
        res.status(200).send({ message: "User added!" });
      })
      .catch(() => {
        res.status(500).send({ message: "server error" });
      });
  },

  getUsers: async (req, res) => {
    try {
      const users = await UserDB.findAll();
      res.status(200).send(users);
    } catch {
      res.status(500).send({ message: "Server error!" });
    }
  },

  updateUser: async (req, res) => {
    const user = await UserDB.findByPk(req.body.id);
    if (user) {
      try {
        user
          .update({
            nume: req.body.nume,
            prenume: req.body.prenume,
            email: req.body.email
          })
          .then(() => {
            res.status(200).send({ message: "Ok, updated user!" });
          });
      } catch {
        res.status(500).send({ message: "server error!" });
      }
    } else {
      res.status(404).send({ message: "User searched does not exist!" });
    }
  },

  deleteUser: async (req, res) => {
    let errors = [];
    await UserDB.findByPk(req.body.id)
      .then(async user => {
        if (user) {
          await user.destroy();
          res.status(200).send({
            message: "User with id: " + req.body.id + " deleted!"
          });
        } else {
          errors.push("There is no user with id: " + req.body.id);
          res.status(400).send(errors);
        }
      })
      .catch(() => {
        res.status(500).send({ message: "Server error!" });
      });
  }
};

module.exports = controller;
