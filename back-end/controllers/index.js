const other = require("./other");
const user = require("./user");

const controllers = {
  user,
  other
};

module.exports = controllers;
