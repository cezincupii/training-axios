const express = require("express");
const bodyParser = require("body-parser");
const app = express();
let port = 8080;
const path = require("path");
const router = require("./routes");
app.use(bodyParser.json());

app.listen(port, () => {
  console.log("Server pornit pe: " + port);
});

//CRUD - CREATE, READ, UPDATE, DELETE
//     - POST  , GET , PUT   , DELETE
// 200 - OK
// 300 - Informational
// 400 - User greseli
// 500 - Erori de server
/*
try {
  app.get("/", (req, res) => {
    res.status(200).send("Merge serveru");
  });
} catch {
  res.status(504).send("Server offline");
}
*/

app.use("/api", router);
//app.render("../front-end/index.js");
app.use("/", express.static("../Front-End/"));
