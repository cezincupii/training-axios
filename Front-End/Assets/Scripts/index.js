let postForm = "postForm";
let getForm = "getForm";
let putForm = "putForm";
let deleteForm = "deleteForm";

const displayForm = idName => {
    let x = document.getElementById(idName);
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
};

